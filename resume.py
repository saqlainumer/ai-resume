import openai
import re
import logging
import json
import tiktoken
import PyPDF2
from flask import Flask, jsonify, request
import docx
import docx2txt
from dotenv import load_dotenv

  
app = Flask(__name__)

load_dotenv()

openai.api_key = os.getenv("api_key")
openai.organization = os.getenv("organization")


def num_tokens_from_string(string: str, model: str) -> int:
    """Returns the number of tokens in a text string."""
    encoding = tiktoken.encoding_for_model(model)
    num_tokens = len(encoding.encode(string))
    return num_tokens


def extract_domain(url):
    parsed_url = urlparse(url)
    if parsed_url.netloc:
        return parsed_url.netloc
    else:
        return None


def convert_json_to_pdf(json_data):
    data = json_data

    pdf_file = "sample.pdf"
    doc = SimpleDocTemplate(pdf_file, pagesize=letter)

    # Create a list of paragraphs from the dictionary data
    paragraphs = []
    styles = getSampleStyleSheet()

    for key, value in data.items():
        text = f"<b>{key}: </b> {value}"
        paragraphs.append(Paragraph(text, styles['Normal']))

    # Build the PDF document with the paragraphs
    doc.build(paragraphs)

    return pdf_file


def log_in_to_linked(driver, user_login, pass_login):


    login_url = 'https://www.linkedin.com/login'
    driver.get(login_url)

    username = driver.find_element(By.ID, 'username')
    username.send_keys(user_login)

    password = driver.find_element(By.ID, 'password')
    password.send_keys(pass_login)

    sign_in_button = driver.find_element(By.XPATH, '//*[@type="submit"]')
    sign_in_button.click()

    return driver


class ResumeParser():
    def __init__(self):

        # GPT-3 completion questions
        self.prompt_questions = \
            """Summarize the text below into a JSON with exactly the following structure {basic_info: {first_name, last_name, full_name, email, phone_number, location, portfolio_website_url, linkedin_url, github_main_page_url, university, education_level (BS, MS, or PhD), graduation_year, graduation_month, majors, GPA}, introduction: {introduction} ,skills: {[technical_skills], non_technical_skills}  , work_experience: [{job_title, company, location, duration, job_summary}], project_experience:[{project_name, project_description}]}
        """
       # set up this parser's logger
        logging.basicConfig(filename='logs/parser.log', level=logging.DEBUG)
        self.logger = logging.getLogger()

    def pdf2string(self: object, pdf_path: str) -> str:
        """
        Extract the content of a pdf file to string.
        :param pdf_path: Path to the PDF file.
        :return: PDF content string.
        """

        pdf_reader = PyPDF2.PdfReader(pdf_path)
        pdf = ""
        for page in pdf_reader.pages:
            pdf += page.extract_text()

        pdf_str = "\n\n".join(pdf)
        pdf_str = re.sub('\s[,.]', ',', pdf_str)
        pdf_str = re.sub('[\n]+', '\n', pdf_str)
        pdf_str = re.sub('[\s]+', ' ', pdf_str)
        pdf_str = re.sub('http[s]?(://)?', '', pdf_str)
        return pdf_str

    def docx2string(self: object, docx_path: str) -> str:
        """
        Extract the content of a .docx file to a string.
        :param docx_path: Path to the .docx file.
        :return: .docx content string.
        """

        doc_str = docx2txt.process(docx_path)

        doc_str = "\n\n".join(doc_str)
        doc_str = re.sub('\s[,.]', ',', doc_str)
        doc_str = re.sub('[\n]+', '\n', doc_str)
        doc_str = re.sub('[\s]+', ' ', doc_str)
        doc_str = re.sub('http[s]?(://)?', '', doc_str)

        return doc_str

    def query_completion(self: object,
                         prompt: str,
                         engine: str = 'gpt-3.5-turbo-16k',
                         temperature: float = 0.0,
                         max_tokens: int = 4096,
                         top_p: int = 1,
                         frequency_penalty: int = 0,
                         presence_penalty: int = 0) -> object:
        """
        Base function for querying GPT-3. 
        Send a request to GPT-3 with the passed-in function parameters and return the response object.
        :param prompt: GPT-3 completion prompt.
        :param engine: The engine, or model, to generate completion.
        :param temperature: Controls the randomnesss. Lower means more deterministic.
        :param max_tokens: Maximum number of tokens to be used for prompt and completion combined.
        :param top_p: Controls diversity via nucleus sampling.
        :param frequency_penalty: How much to penalize new tokens based on their existence in text so far.
        :param presence_penalty: How much to penalize new tokens based on whether they appear in text so far.
        :return: GPT-3 response object
        """

        estimated_prompt_tokens = num_tokens_from_string(prompt, engine)
        estimated_answer_tokens = (max_tokens - estimated_prompt_tokens)
        self.logger.info(
            f'Tokens: {estimated_prompt_tokens} + {estimated_answer_tokens} = {max_tokens}')

        response = openai.ChatCompletion.create(
            model=engine,  # Use ChatCompletion for chat models
            messages=[
                {"role": "system", "content": "You are a helpful assistant that extracts resume information."},
                {"role": "user", "content": prompt},
            ],
            temperature=temperature,
            max_tokens=estimated_answer_tokens,
            top_p=top_p,
            frequency_penalty=frequency_penalty,
            presence_penalty=presence_penalty
        )
        return response

    def query_resume(self: object, file: str) -> dict:
        """
        Query GPT-3 for the work experience and / or basic information from the resume at the PDF file path.
        :param pdf_path: Path to the PDF file.
        :return dictionary of resume with keys (basic_info, work_experience).
        """

        file_extension = os.path.splitext(file)[-1].lower()
        _str = ""

        # Check the file extension and call the appropriate function
        if file_extension == '.pdf':
            _str = self.pdf2string(file)
        else:
            _str = self.docx2string(file)

        resume = {}
        prompt = self.prompt_questions + '\n' + _str

        # Reference: https://platform.openai.com/docs/models/gpt-3-5
        engine = 'gpt-3.5-turbo-16k'
        max_tokens = 4096

        response = self.query_completion(
            prompt, engine=engine, max_tokens=max_tokens)
        response_text = response['choices'][0]['message']['content'].strip()
        resume = json.loads(response_text)
        return resume


class airesume():
    def __init__(self, job_description):

        self.job_description = job_description

        # GPT-3 completion questions
        self.prompt_questions = f"""You are an expert technical writer. For the following job description that is delimited by \
        triple backticks, extract the following information:

        skills: what are the skills required for the job description? \
        The skils must showcase that I am the ideal candidate for the job description provided. Return list of skills.  

        experience: write relevant experience that makes me the ideal candidate for this role described in job description. \
        The experience must showcase that I am the ideal candidate for the job description provided.  

        introduction: Compose a formal and compelling introduction for a professional resume that effectively conveys my background, qualifications, and enthusiasm for  \
        this role.Tailor the introduction to align with the specific requirements and objectives outlined in the \
        job description. Also keep in view the skills and experience mentioned above. Try to include action verbs, \
        give tangible and concrete examples, and include success metrics when available. Donot use name in the introduction. \
        keep the introduction formal as written in the resume.  Aim for a concise and impactful opening that grabs the reader's  \
        attention and sets a positive tone for the rest of your resume. Avoid starting with 'I am' and instead use phrases such as  \
        'I possess,' 'With a background in,' 'A dedicated and accomplished,' 'Experienced in,' 'Proficient in,' or 'Skilled in.'

        Format the output as JSON with the following keys:
        skills
        experience
        introduction

 job description:  ```{self.job_description}```
        """

       # set up this parser's logger
        logging.basicConfig(filename='logs/parser.log', level=logging.DEBUG)
        self.logger = logging.getLogger()

    def query_completion(self: object,
                         prompt: str,
                         engine: str = 'gpt-3.5-turbo-16k',
                         temperature: float = 0.0,
                         max_tokens: int = 4096,
                         top_p: int = 1,
                         frequency_penalty: int = 0,
                         presence_penalty: int = 0) -> object:
        """
        Base function for querying GPT-3. 
        Send a request to GPT-3 with the passed-in function parameters and return the response object.
        :param prompt: GPT-3 completion prompt.
        :param engine: The engine, or model, to generate completion.
        :param temperature: Controls the randomnesss. Lower means more deterministic.
        :param max_tokens: Maximum number of tokens to be used for prompt and completion combined.
        :param top_p: Controls diversity via nucleus sampling.
        :param frequency_penalty: How much to penalize new tokens based on their existence in text so far.
        :param presence_penalty: How much to penalize new tokens based on whether they appear in text so far.
        :return: GPT-3 response object
        """

        estimated_prompt_tokens = num_tokens_from_string(prompt, engine)
        estimated_answer_tokens = (max_tokens - estimated_prompt_tokens)
        self.logger.info(
            f'Tokens: {estimated_prompt_tokens} + {estimated_answer_tokens} = {max_tokens}')

        response = openai.ChatCompletion.create(
            model=engine,  # Use ChatCompletion for chat models
            messages=[
                {"role": "system", "content": "You are an excellent technical writer that writes resume using given jon information."},
                {"role": "user", "content": prompt},
            ],
            temperature=temperature,
            max_tokens=estimated_answer_tokens,
            top_p=top_p,
            frequency_penalty=frequency_penalty,
            presence_penalty=presence_penalty
        )
        return response

    def parse_desc(self: object) -> dict:
        """
        Query GPT-3 for the work experience and / or basic information from the resume at the PDF file path.
        :param pdf_path: Path to the PDF file.
        :return dictionary of resume with keys (basic_info, work_experience).
        """
        prompt = self.prompt_questions + '\n'

        # Reference: https://platform.openai.com/docs/models/gpt-3-5
        engine = 'gpt-3.5-turbo-16k'
        max_tokens = 4096

        response = self.query_completion(
            prompt, engine=engine, max_tokens=max_tokens)
        response_text = response['choices'][0]['message']['content'].strip()
        resume = json.loads(response_text)
        return resume


def get_data_from_indeed(driver, url):

    pattern = r'viewjob'

    title_class = ""
    company_class = ""
    location_class = ""
    jd_class = ""

    if re.search(pattern, url):
        print("view job")
        title_class = "#viewJobSSRRoot > div > div.css-1quav7f.eu4oa1w0 > div > div > div.jobsearch-JobComponent.css-u4y1in.eu4oa1w0.jobsearch-JobComponent-bottomDivider > div.jobsearch-InfoHeaderContainer.jobsearch-DesktopStickyContainer.css-zt53js.eu4oa1w0 > div:nth-child(1) > div.jobsearch-JobInfoHeader-title-container.css-bbq8li.eu4oa1w0 > h1 > span"
        company_class = "#viewJobSSRRoot > div > div.css-1quav7f.eu4oa1w0 > div > div > div.jobsearch-JobComponent.css-u4y1in.eu4oa1w0.jobsearch-JobComponent-bottomDivider > div.jobsearch-InfoHeaderContainer.jobsearch-DesktopStickyContainer.css-zt53js.eu4oa1w0 > div:nth-child(1) > div.css-2wyr5j.eu4oa1w0 > div > div > div > div.css-1h46us2.eu4oa1w0 > div > span > a"
        location_class = "#viewJobSSRRoot > div > div.css-1quav7f.eu4oa1w0 > div > div > div.jobsearch-JobComponent.css-u4y1in.eu4oa1w0.jobsearch-JobComponent-bottomDivider > div.jobsearch-InfoHeaderContainer.jobsearch-DesktopStickyContainer.css-zt53js.eu4oa1w0 > div:nth-child(1) > div.css-2wyr5j.eu4oa1w0 > div > div > div > div:nth-child(2) > div"
        jd_class = "#jobDescriptionText"
    else:
        print("small window")
        title_class = "#vjs-container > div > div > div > div.jobsearch-HeaderContainer > div > div:nth-child(1) > div.jobsearch-JobInfoHeader-title-container.css-bbq8li.eu4oa1w0 > h2 > span"
        company_class = "#vjs-container > div > div > div > div.jobsearch-HeaderContainer > div > div:nth-child(1) > div.css-2wyr5j.eu4oa1w0 > div > div > div > div.css-1h46us2.eu4oa1w0 > div > span"
        location_class = "#vjs-container > div > div > div > div.jobsearch-HeaderContainer > div > div:nth-child(1) > div.css-2wyr5j.eu4oa1w0 > div > div > div > div:nth-child(2)"
        jd_class = "#jobDescriptionText"

    driver.get(url)

    # Wait for the job title element to be present
    title_element = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, title_class)))

    # Wait for the company element to be present
    company_element = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, company_class)))

    # Wait for the location element to be present
    location_element = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, location_class)))

    # Wait for the job description element to be present
    jd_element = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, jd_class)))

    job_title = title_element.text
    company = company_element.text
    location = location_element.text
    job_description = jd_element.text

    data = {'job_title': job_title, 'company': company,
            'location': location, 'job_description': job_description}

    driver.quit()

    return data


def get_data_from_monster(driver, url):

    driver.get(url)

    title_class = "#BigJobCardId > div.headerstyle__JobViewHeaderContainer-sc-1ijq9nh-0.jsxEyA > div > div.headerstyle__JobViewHeaderLeft-sc-1ijq9nh-3.fPFMyN > div.headerstyle__JobViewHeaderContent-sc-1ijq9nh-8.fpYqik > h1"
    company_class = "#BigJobCardId > div.headerstyle__JobViewHeaderContainer-sc-1ijq9nh-0.jsxEyA > div > div.headerstyle__JobViewHeaderLeft-sc-1ijq9nh-3.fPFMyN > div.headerstyle__JobViewHeaderContent-sc-1ijq9nh-8.fpYqik > a > h2"
    location_class = "#BigJobCardId > div.headerstyle__JobViewHeaderContainer-sc-1ijq9nh-0.jsxEyA > div > div.headerstyle__JobViewHeaderLeft-sc-1ijq9nh-3.fPFMyN > div.headerstyle__JobViewHeaderContent-sc-1ijq9nh-8.fpYqik > h3"
    jd_class = "#jobview-container > div.jobview-containerstyles__JobViewContent-sc-16af7k7-4.fJxmcm > div > div.descriptionstyles__DescriptionContainer-sc-13ve12b-0.iCEVUR"

    # Wait for the job title element to be present
    title_element = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, title_class)))

    company = ""
    try:
        company_element = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, company_class)))
        company = company_element.text
    except:
        print("Company Name is confidential")
    finally:
        if len(company) == 0:
            company = "Confidential"

    # Wait for the location element to be present
    location_element = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, location_class)))

    # Wait for the job description element to be present
    jd_element = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, jd_class)))

    job_title = title_element.text
    location = location_element.text
    job_description = jd_element.text

    data = {'job_title': job_title, 'company': company,
            'location': location, 'job_description': job_description}

    driver.quit()

    return data


def get_data_from_linkedin(driver, url):

    title_class = "body > div.application-outlet > div.authentication-outlet > div > div.job-view-layout.jobs-details > div.grid > div > div:nth-child(1) > div > div > div.p5 > div.display-flex.justify-space-between.flex-wrap > h1"
    company_class = "body > div.application-outlet > div.authentication-outlet > div > div.job-view-layout.jobs-details > div.grid > div > div:nth-child(1) > div > div > div.p5 > div.job-details-jobs-unified-top-card__primary-description > div > a"
    location_class = "body > div.application-outlet > div.authentication-outlet > div > div.job-view-layout.jobs-details > div.grid > div > div:nth-child(1) > div > div > div.p5 > div.job-details-jobs-unified-top-card__primary-description > div"
    jd_class = "#job-details > span"

    driver.get(url)
    location_pattern = r'·\s(.*?)\s\d+\s'

    # Wait for the job title element to be present
    title_element = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, title_class)))

    # Wait for the company element to be present
    company_element = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, company_class)))

    # Wait for the location element to be present
    location_element = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, location_class)))

    match = re.search(location_pattern, location_element.text)

    location = ""
    if match:
        location = match.group(1)
        words = location.split()

        if words[-1] == "Reposted":
            words.pop()

        result_string = ' '.join(words)

        location = result_string
    else:
        location = "No Location Found"

    # Wait for the job description element to be present
    jd_element = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, jd_class)))

    job_title = title_element.text
    company = company_element.text
    job_description = jd_element.text

    driver.quit()

    data = {'job_title': job_title, 'company': company,
            'location': location, 'job_description': job_description}

    return data


def get_job_desc(link, domain, username, password):
    driver = webdriver.Chrome()
    job_desc = None
    
    if domain == "pk.indeed.com":
        job_desc = get_data_from_indeed(driver, link)
    elif domain == "www.monster.com":
        job_desc = get_data_from_monster(driver, link)
    elif domain == "www.careerbuilder.com":
        print("careerbuilder NOT implemented, ROADBLOCK")
    elif domain == "www.linkedin.com":
        if len(username) == 0 or len(password) == 0:
            print("You Must Provide username and password")
        else:
            driver_instance = log_in_to_linked(driver, username, password)
            job_desc = get_data_from_linkedin(driver_instance, link)
    else:
        print("INVALID URL")
    
    return job_desc




@app.route('/parse_resume', methods=['POST'])
def upload_file():
    if 'file' not in request.files:
        return jsonify({'error': 'No file part'})

    file = request.files['file']
    link = request.form.get('link')
    username = request.form.get('username')
    password = request.form.get('password')

    if file.filename == '':
        return jsonify({'error': 'No selected file'})
    elif link is None or link.strip() == "":
        return jsonify({'error': 'Job link is Invalid'})

    if file and (file.filename.endswith('.pdf') or file.filename.endswith('.docx')):
        try:
            start_time = time.time()

            print("parse resume")
            json_resume = ResumeParser_obj.query_resume(file.filename)
            
            print("get domain name")
            domain = extract_domain(link)
            
            print("get jon info")
            job_desc = get_job_desc(link, domain, username, password)


            print("parse description")
            airesume_obj = airesume(job_desc)
            ai_resume = airesume_obj.parse_desc()

            json_resume["skills"]["technical_skills"] = ai_resume["skills"]
            json_resume["introduction"]["introduction"] = ai_resume["introduction"]

            print("convert to json")
            pdf_file = convert_json_to_pdf(json_resume)

            end_time = time.time()
            elapsed_time = end_time - start_time

            print(f"the whole process took {elapsed_time: .2f} seconds to run.")

            return send_file(pdf_file, as_attachment=True)
            # return jsonify({'information': json_resume, "job_desc": job_desc, "ai_resume": ai_resume})
        except Exception as e:
            return jsonify({'error': str(e)})

    else:
        return jsonify({'error': 'Invalid file format. Please upload a valid PDF/docx file'})


# curl -X POST -F "file=@path/to/your/pdf.pdf" http://localhost:5000/parse_resume


if __name__ == '__main__':
    ResumeParser_obj = ResumeParser()
    app.run(debug=True)




